package com.stackOverFlow;

import org.bson.types.ObjectId;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.mongo.UpdateOptions;

public class QAPostVerticle extends AbstractVerticle {
	
	@Override
	public void start() throws Exception {
		vertx.eventBus().consumer("post.question", message -> {
			System.out.println("QAPostVerticle.start() POST QUESTION message.body().toString(): "+message.body().toString());
			JsonObject object = new JsonObject(message.body().toString());
			String mongodbHost = object.getString(DefaultQAService.MONGODB_HOST);
			object.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
//			config.put("connection_string", "mongodb://" + QAReadService.MONGODB + ":27017");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			client.insert("questions", object, res -> {
				if (res.succeeded()) {
					System.out.println("QAPostVerticle.start() res.result(): "+res.result());
					message.reply("Question is POSTED with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() res.cause()L "+res.cause());
					message.fail(500, "Question Posting failed");
				}
			});
		});
		
		vertx.eventBus().consumer("edit.question", message -> {
			System.out.println("QAPostVerticle.start() edit.question message.body().toString(): "+message.body().toString());
			JsonObject jsonObj = new JsonObject(message.body().toString());
			String mongodbHost = jsonObj.getString(DefaultQAService.MONGODB_HOST);
			jsonObj.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
//			config.put("connection_string", "mongodb://" + QAReadService.MONGODB + ":27017");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(true);
			JsonObject query = new JsonObject().put("_id", jsonObj.getString("questionId"));
			JsonObject update = new JsonObject().put("$set", new JsonObject().put("title", jsonObj.getString("title")).put("description",
					jsonObj.getString("description")).put("lastUpdated", jsonObj.getString("lastUpdated")));
			client.updateCollectionWithOptions("questions", query, update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
		});
		
		vertx.eventBus().consumer("edit.answer", message -> {
			System.out.println("QAPostVerticle.start() edit.answer message.body().toString(): "+message.body().toString());
			JsonObject jsonObj = new JsonObject(message.body().toString());
			String mongodbHost = jsonObj.getString(DefaultQAService.MONGODB_HOST);
			jsonObj.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
//			config.put("connection_string", "mongodb://" + QAReadService.MONGODB + ":27017");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(true);
//			JsonObject query = new JsonObject().put("_id", jsonObj.getString("questionId"));
			JsonObject query = new JsonObject().put("_id", jsonObj.getString("questionId")).put("answers._id", jsonObj.getString("answerId"));
			JsonObject update = new JsonObject().put("$set", new JsonObject().put("answers.$.description", jsonObj.getString("description")).put("answers.$.lastUpdated", jsonObj.getString("lastUpdated")));
			client.updateCollectionWithOptions("questions", query, update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
		});
		
		vertx.eventBus().consumer("comment.question", message -> {
			System.out.println("QAPostVerticle.start() comment.question message.body().toString(): "+message.body().toString());
			JsonObject jsonCommentObj = new JsonObject(message.body().toString());
			String mongodbHost = jsonCommentObj.getString(DefaultQAService.MONGODB_HOST);
			jsonCommentObj.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(true);
			String questionId = jsonCommentObj.getString("questionId");
			jsonCommentObj.remove("questionId");
			jsonCommentObj.put("_id", new ObjectId().toString());
			JsonObject query = new JsonObject().put("_id", questionId);
			JsonObject update = new JsonObject().put("$push", new JsonObject().put("comments", jsonCommentObj));
			client.updateCollectionWithOptions("questions", query, update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
		});
		
		vertx.eventBus().consumer("vote.question", message -> {
			System.out.println("QAPostVerticle.start() vote.question message.body().toString(): "+message.body().toString());
			JsonObject jsonCommentObj = new JsonObject(message.body().toString());
			String mongodbHost = jsonCommentObj.getString(DefaultQAService.MONGODB_HOST);
			jsonCommentObj.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			String questionId = jsonCommentObj.getString("questionId");
			jsonCommentObj.remove("questionId");
			
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(true);
			JsonObject query = new JsonObject().put("_id", questionId);
			JsonObject update = new JsonObject().put("$push", new JsonObject().put("vote", jsonCommentObj.getJsonObject("user")));
			client.updateCollectionWithOptions("questions", query, update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
			
		});
		
		vertx.eventBus().consumer("comment.answer", message -> {
			System.out.println("QAPostVerticle.start() comment.answer message.body().toString(): "+message.body().toString());
			JsonObject jsonCommentObj = new JsonObject(message.body().toString());
			String mongodbHost = jsonCommentObj.getString(DefaultQAService.MONGODB_HOST);
			jsonCommentObj.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(true);
			String questionId = jsonCommentObj.getString("questionId");
			String answerId = jsonCommentObj.getString("answerId");
			jsonCommentObj.remove("questionId");
			jsonCommentObj.remove("answerId");
			jsonCommentObj.put("_id", new ObjectId().toString());
//			JsonObject query = new JsonObject().put("answers._id", answerId);
			JsonObject query = new JsonObject().put("_id", questionId).put("answers._id", answerId);
//			JsonObject answer_query = new JsonObject().put("_id", answerId);
//			query.put("answers", new JsonArray().add(answer_query));
			System.out.println("QAPostVerticle.start() query.encodePrettily(): "+query.encodePrettily());
//			JsonObject update = new JsonObject().put("$push", new JsonObject().put("comments", jsonCommentObj));
			JsonObject update = new JsonObject().put("$push", new JsonObject().put("answers.$.comments", jsonCommentObj));
			
			client.updateCollectionWithOptions("questions", query, update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
		});
		
		vertx.eventBus().consumer("answer.question", message -> {
			System.out.println("QAPostVerticle.start() answer.question message.body().toString(): "+message.body().toString());
			JsonObject jsonAnswerObj = new JsonObject(message.body().toString());
			String mongodbHost = jsonAnswerObj.getString(DefaultQAService.MONGODB_HOST);
			jsonAnswerObj.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(true);
			String questionId = jsonAnswerObj.getString("questionId");
			jsonAnswerObj.remove("questionId");
			JsonObject query = new JsonObject().put("_id",questionId);
			jsonAnswerObj.put("_id", new ObjectId().toString());
			JsonObject update = new JsonObject().put("$push", new JsonObject().put("answers", jsonAnswerObj));
			client.updateCollectionWithOptions("questions", query, update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
		});
		
		vertx.eventBus().consumer("post.answer", message -> {
			JsonObject msgJsonObj = new JsonObject(message.body().toString());
			String mongodbHost = msgJsonObj.getString(DefaultQAService.MONGODB_HOST);
			msgJsonObj.remove(DefaultQAService.MONGODB_HOST);
			String questionId = msgJsonObj.getString("questionId");
			System.out.println("QAPostVerticle.start() questionId: "+questionId);
			JsonObject answerObj = msgJsonObj.getJsonObject("answer");
			System.out.println("QAPostVerticle.start() answerObj.encodePrettily(): "+answerObj.encodePrettily());
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
//			JsonObject object = new JsonObject(message.body().toString());
//			JsonObject update = new JsonObject().put("$push", new JsonObject().put("answers", new JsonObject().put("$each", answerObj)));
			JsonObject update = new JsonObject().put("$push", new JsonObject().put("answers", answerObj));
			System.out.println("QAPostVerticle.start() update.encodePrettily(): "+update.encodePrettily());
			JsonObject query = new JsonObject().put("_id", questionId);
			client.updateCollection("questions", query,update, res -> {
				if (res.succeeded()) {
					message.reply("Question is POSTED with id " + res.result());
				} else {
					message.fail(500, "Question Posting failed");
				}
			});
		});
		
		vertx.eventBus().consumer("update.user.info", message -> {
			System.out.println("QAPostVerticle.start() update.user.info message.body().toString(): "+message.body().toString());
			JsonObject jsonObj = new JsonObject(message.body().toString());
			String mongodbHost = jsonObj.getString(DefaultQAService.MONGODB_HOST);
			jsonObj.remove(DefaultQAService.MONGODB_HOST);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			UpdateOptions updateOptions = new UpdateOptions().setUpsert(false).setMulti(true);
			
//			updating the user info for the users who posted the questions
			JsonObject ques_query = new JsonObject().put("user.username", jsonObj.getString("username"));
			JsonObject ques_update = new JsonObject().put("$set", new JsonObject().put("user.name", jsonObj.getString("name")));
			client.updateCollectionWithOptions("questions", ques_query, ques_update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() UPDATED for QUESTIONS res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED for QUESTIONS with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() UPDATED for QUESTIONS FAILED res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
			
//			updating the user info for the users who posted the comments over questions
//			JsonObject ques_comment_query = new JsonObject().put("comments.user.username", jsonObj.getString("username"));
//			JsonObject ques_comment_update = new JsonObject().put("$set", new JsonObject().put("comments.$.user.name", jsonObj.getString("name")).put("comments.$.user.city", jsonObj.getString("city")));
//			client.updateCollectionWithOptions("questions", ques_comment_query, ques_comment_update, updateOptions, res -> {
//				System.out.println("QAPostVerticle.start() UPDATED for QUESTIONS COMMENTS res.succeeded(): "+res.succeeded());
//				if (res.succeeded()) {
//					message.reply("Question is UPDATED for QUESTIONS COMMENTS with id " + res.result());
//				} else {
//					System.out.println("QAPostVerticle.start() UPDATED for QUESTIONS COMMENTS FAILED res.cause(): "+res.cause());
//					message.fail(500, "Question UPDATE failed");
//				}
//			});
			
//			updating the user info for the users who posted the answers
			JsonObject ans_query = new JsonObject().put("answers.user.username", jsonObj.getString("username"));
			JsonObject ans_update = new JsonObject().put("$set", new JsonObject().put("answers.$.user.name", jsonObj.getString("name")).put("answers.$.user.city", jsonObj.getString("city")));
			client.updateCollectionWithOptions("questions", ans_query, ans_update, updateOptions, res -> {
				System.out.println("QAPostVerticle.start() UPDATED ANSWERS res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					message.reply("Question is UPDATED for ANSWERS with id " + res.result());
				} else {
					System.out.println("QAPostVerticle.start() UPDATED for ANSWERS FAILED res.cause(): "+res.cause());
					message.fail(500, "Question UPDATE failed");
				}
			});
			
//			updating the user info for the users who posted the comments over questions
//			JsonObject ans_comment_query = new JsonObject().put("answers.comments.user.username", jsonObj.getString("username"));
//			JsonObject ans_comment_update = new JsonObject().put("$set", new JsonObject().put("answers.$.comments.$.user.name", jsonObj.getString("name")).put("answers.$.comments.$.user.city", jsonObj.getString("city")));
//			client.updateCollectionWithOptions("questions", ans_comment_query, ans_comment_update, updateOptions, res -> {
//				System.out.println("QAPostVerticle.start() for ANSWERS COMMENTS res.succeeded(): "+res.succeeded());
//				if (res.succeeded()) {
//					message.reply("Question is UPDATED for ANSWERS COMMENTS with id " + res.result());
//				} else {
//					System.out.println("QAPostVerticle.start() UPDATED ANSWERS COMMENTS FAILED res.cause(): "+res.cause());
//					message.fail(500, "Question UPDATE failed");
//				}
//			});
		});
	}

}
