package com.stackOverFlow;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;

public class QAReadVerticle extends AbstractVerticle {

	@Override
	public void start() throws Exception {

		vertx.eventBus().consumer("read.Question", message -> {
			JsonObject queryObj = new JsonObject();
			FindOptions options = new FindOptions().setFields(
					new JsonObject().put("_id", true).put("title", true).put("description", true).put("user", true).put("asked", true).put("comments", true).put("vote", true));
//			if (message != null && message.body() != null && message.body().toString().length() > 0) {
			System.out.println("QAReadVerticle.start() message: "+message);
			System.out.println("QAReadVerticle.start() message.body(): "+message.body());
				JsonObject msgJsonObj = new JsonObject(message.body().toString());
				String mongodbHost = msgJsonObj.getString(DefaultQAService.MONGODB_HOST);
				
				String questionId = msgJsonObj.getString("questionId");
				String search_query = msgJsonObj.getString("search_query");
//				System.out.println("QAReadVerticle.start() questionId: " + questionId);
//				System.out.println("QAReadVerticle.start() search_query: " + search_query);
				if (questionId != null && questionId.length() > 0) {
					options.getFields().put("answers", true);
					queryObj = new JsonObject().put("_id", questionId);
				} else if (search_query != null && search_query.length() > 0) {
					queryObj = new JsonObject().put("title",
							new JsonObject().put("$regex", ".*" + search_query + ".*").put("$options", "i"));
				}
//			}
			System.out.println("QAReadVerticle.start() queryObj: " + queryObj.encodePrettily());
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			client.findWithOptions("questions", queryObj, options, res -> {
				System.out.println("QAReadVerticle.start() res.succeeded(): "+res.succeeded());
				if (res.succeeded()) {
					List<JsonObject> resultList = res.result();
					for (int i = 0; i < resultList.size(); i++) {
						if(questionId != null && questionId.length() > 0) {
//							Only for question details page
							JsonObject quesObj = resultList.get(i);
//							formatting posted date of questions 
							modifyDate(quesObj, "asked");
							JsonArray comments = quesObj.getJsonArray("comments");
							if(comments != null) {
//								formatting date of comments on questions
								for (int j = 0; j < comments.size(); j++) {
									JsonObject commentObj= comments.getJsonObject(j);
									modifyDate(commentObj,"commented");
								}
							}
							JsonArray answers = quesObj.getJsonArray("answers");
//							formatting answered date
							if(answers != null) {
								for (int k = 0; k < answers.size(); k++) {
									JsonObject answerObj= answers.getJsonObject(k);
									modifyDate(answerObj,"answered");
									JsonArray comments_on_answer = answerObj.getJsonArray("comments");
									if(comments_on_answer != null) {
//										formatting date of comments on answers
										for (int j = 0; j < comments_on_answer.size(); j++) {
											JsonObject answer_commentObj= comments_on_answer.getJsonObject(j);
											modifyDate(answer_commentObj,"commented");
										}
									}
								}
							}
						} else {
//							For all queations and search result questions
							JsonObject quesObj = resultList.get(i);
//							formatting posted date of questions 
							modifyDate(quesObj, "asked");
						}
					}
					message.reply(new JsonArray(res.result()));
				} else {
					res.cause().printStackTrace();
					message.fail(500, "NO Questions found");
				}
			});
		});
		
		vertx.eventBus().consumer("read.Question.newest", message -> {
			System.out.println("QAReadVerticle.start() handling NEWEST");
			JsonObject queryObj = new JsonObject();
			FindOptions options = new FindOptions().setFields(new JsonObject().put("_id", true).put("title", true)
					.put("description", true).put("user", true).put("asked", true).put("comments", true).put("vote", true)).setSort(new JsonObject().put("asked",-1)).setLimit(10);
			JsonObject msgJsonObj = new JsonObject(message.body().toString());
			String mongodbHost = msgJsonObj.getString(DefaultQAService.MONGODB_HOST);

			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			client.findWithOptions("questions", queryObj, options, res -> {
				System.out.println("QAReadVerticle.start() res.succeeded(): " + res.succeeded());
				if (res.succeeded()) {
					List<JsonObject> resultList = res.result();
					for (int i = 0; i < resultList.size(); i++) {
						JsonObject quesObj = resultList.get(i);
						modifyDate(quesObj,"asked");
					}
					message.reply(new JsonArray(res.result()));
				} else {
					res.cause().printStackTrace();
					message.fail(500, "NO Questions found");
				}
			});
		});
		
		vertx.eventBus().consumer("questions.me", message -> {
			System.out.println("QAReadVerticle.start() handling MY QUESTIONS");
			FindOptions options = new FindOptions().setFields(new JsonObject().put("_id", true).put("title", true)
					.put("description", true).put("user", true).put("asked", true).put("comments", true)).setSort(new JsonObject().put("asked",-1));
			JsonObject msgJsonObj = new JsonObject(message.body().toString());
			String mongodbHost = msgJsonObj.getString(DefaultQAService.MONGODB_HOST);
			String userid = msgJsonObj.getString("username");
			JsonObject queryObj = new JsonObject().put("user.username", userid);
			JsonObject config = new JsonObject();
			config.put("db_name", "stackoverflow");
			config.put("connection_string", "mongodb://" + mongodbHost + DefaultQAService.MONGO_PORT);
			MongoClient client = MongoClient.createShared(vertx, config);
			client.findWithOptions("questions", queryObj, options, res -> {
				System.out.println("QAReadVerticle.start() MY QUESTIONS res.succeeded(): " + res.succeeded());
				if (res.succeeded()) {
					List<JsonObject> resultList = res.result();
					for (int i = 0; i < resultList.size(); i++) {
						JsonObject quesObj = resultList.get(i);
						modifyDate(quesObj,"asked");
					}
					message.reply(new JsonArray(res.result()));
				} else {
					res.cause().printStackTrace();
					message.fail(500, "NO Questions found");
				}
			});
		});
		
	}
	
	private JsonObject modifyDate(JsonObject jsonObj, String key) {
		System.out.println("QAReadVerticle.modifyDate() jsonObj.encodePrettily(): "+jsonObj.encodePrettily());
		System.out.println("QAReadVerticle.modifyDate() key: "+key);
		System.out.println("QAReadVerticle.modifyDate() jsonObj.getString(key): "+jsonObj.getString(key));
		if(jsonObj.getString(key) == null) {
			return jsonObj;
		}
		Instant asked = Instant.parse(jsonObj.getString(key));
		System.out.println("QAReadVerticle.modifyDate() postedDate: "+asked);
		LocalDateTime date_time = LocalDateTime.ofInstant(asked, ZoneId.systemDefault());
		System.out.println("QAReadVerticle.start() getDayOfMonth: "+date_time.getDayOfMonth() + " getDayOfYear: " + date_time.getDayOfYear() + 
				" getHour: " + date_time.getHour() + " getMinute: " + date_time.getMinute()+ " getMonthValue: " + date_time.getMonthValue()+
				" getYear: " + date_time.getYear()+ " getDayOfWeek: " + date_time.getDayOfWeek() + " getMonth: " + date_time.getMonth());
		long seconds = ChronoUnit.SECONDS.between(asked, Instant.now());
		System.out.println("QAReadVerticle.start() seconds: "+seconds);
		if(seconds > 0 && seconds < 60) {
			jsonObj.put(key, key + " " + seconds + " secs ago");
		} else if (seconds >= 60 ) {
			long minutes = ChronoUnit.MINUTES.between(asked, Instant.now());
			if(minutes < 60) {
				jsonObj.put(key, key + " " +  minutes + " minutes ago");
			} else if(minutes > 60) {
				long hours = ChronoUnit.HOURS.between(asked, Instant.now());
				if(hours < 24) {
					jsonObj.put(key, key + " " + hours + " hours ago");
				} else {
//					date and time in year-date-hour-minutes
					String value = getYear_Month_time(key,date_time);
					jsonObj.put(key, value);
				}
			}
		} else if (seconds < 0) {
//			date and time in year-date-hour-minutes
			String value = getYear_Month_time(key,date_time);
			jsonObj.put(key, value);
		}else if (seconds == 0) {
			jsonObj.put(key, key + " " + " just a while ago");
		}
		return jsonObj;
	}
	
	private String getYear_Month_time(String key,LocalDateTime date_time) {
		return key + " " + date_time.getMonth() + " " + date_time.getDayOfMonth() + " " + date_time.getYear() + " " + " at " + date_time.getHour() + ":" + date_time.getMinute();
	}

}
