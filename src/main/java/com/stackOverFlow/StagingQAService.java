package com.stackOverFlow;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.ClusteredSessionStore;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class StagingQAService extends DefaultQAService {

	private static final String SERVICE_NAME = "qaservice";

	@Override
	protected String getMongoDBHost() {
		//return "localhost";
		 return "mongodbhost";
	}

	@Override
	protected void createSessionHandler(Router router) {
		router.route().handler(SessionHandler.create(ClusteredSessionStore.create(vertx)));
	}

	@Override
	protected void getHttpClient(Handler<AsyncResult<Object>> handler) {
		HttpEndpoint.getClient(discovery, new JsonObject().put("name", "userservice"), ar -> {
			if (ar.failed()) {
				System.out.println("userservice Server Not Ready!");
				handler.handle(Future.failedFuture("userservice Server Not Ready!"));
			} else {
				HttpClient client = ar.result();
				handler.handle(Future.succeededFuture(client));
			}
		});
	}

	@Override
	protected void doAuthorization(RoutingContext context, HttpClient client, String path,
			Handler<AsyncResult<Object>> handler) {
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {
			HttpClientRequest toReq = client.request(context.request().method(), path, response -> {
				response.bodyHandler(body -> {
					System.out
							.println("StagingQAService.doAuthorization() response.statusCode(): " + response.statusCode());
					if (response.statusCode() >= 500) { // api endpoint server
														// error, circuit
														// breaker
														// should fail
						future.fail(response.statusCode() + ": " + body.toString());
						handler.handle(Future.failedFuture(response.statusCode() + ": " + body.toString()));
					} else {
						System.out.println("StagingQAService.doAuthorization() body: " + body);
						JsonObject userObj = body.toJsonObject();
						System.out.println("StagingQAService.doAuthorization()" + userObj.getString("username"));
//						handler.handle(Future
//								.succeededFuture(response.statusCode() + ":" + usernameObj.getString("username")));
						handler.handle(Future
								.succeededFuture(getUserDetails(response.statusCode(),userObj)));
						future.complete();
					}
					ServiceDiscovery.releaseServiceObject(discovery, client);
				});
			});
			// set headers
			context.request().headers().forEach(header -> {
				toReq.putHeader(header.getKey(), header.getValue());
			});
			if (context.user() != null) {
				toReq.putHeader("user-principal", context.user().principal().encode());
			}
			System.out.println("StagingQAService.doAuthorization() context.getBody(): " + context.getBody());
			// send request
			if (context.getBody() == null) {
				toReq.end();
			} else {
				toReq.end(context.getBody());
			}
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
				handler.handle(Future.failedFuture("bad_gateway"));
			}
		});
	}

	@Override
	protected void createHttpServer(Router router, Future<Void> future) {
		String host = config().getString("http.address", "localhost");
		int port = config().getInteger("http.port", DEFAULT_PORT);
		System.out.println("StagingQAService.start() host: " + host);
		// create HTTP server and publish REST service
		createHttpServer(router, host, port)
				.compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port, "/questions/"));
	}

}
