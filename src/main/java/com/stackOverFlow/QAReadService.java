package com.stackOverFlow;

import java.util.Date;
import java.util.StringTokenizer;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.types.HttpEndpoint;

public class QAReadService extends RestAPIVerticle {

//	public static final String MONGODB = "localhost";

	// public static final String MONGODB = "mongodbhost";

	private static final int DEFAULT_PORT = 8090;

	private static final String SERVICE_NAME = "qareadservice";

	 public static final String MONGODB = "104.197.175.52";

	public static final String MONGO_PORT = ":27017";
	// public static final String MONGO_PORT = ":8080";

	public static final String EDIT_TYPE = "edit";

	public static final String COMMENT_TYPE = "comment";
	
	private static final String USER_SERVICE_HOST = "104.154.248.9";

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		super.start();
		String host = config().getString("http.address", "localhost");
		int port = config().getInteger("http.port", DEFAULT_PORT);
		System.out.println("QAReadService.start() host: " + host);
		Router router = Router.router(vertx);
		vertx.deployVerticle("com.stackOverFlow.QAReadVerticle",
				new DeploymentOptions().setWorker(true).setInstances(2));
		vertx.deployVerticle("com.stackOverFlow.QAPostVerticle",
				new DeploymentOptions().setWorker(true).setInstances(2));

		// body handler
		router.route().handler(BodyHandler.create());
		router.route().handler(CookieHandler.create());
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));

		router.get("/questions/").handler(rctx -> {
			vertx.eventBus().send("read.Question", null, res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/questions/results/").handler(rctx -> {
			System.out.println("QAReadService.start() URI: " + rctx.request().uri());
			System.out.println("QAReadService.start() search_query: " + rctx.request().getParam("search_query"));
			JsonObject jsonObj = new JsonObject().put("search_query", rctx.request().getParam("search_query"));
			vertx.eventBus().send("read.Question", jsonObj.encodePrettily(), res -> {
				System.out.println(
						"QAReadService.start() res.succeeded(): " + res.succeeded() + " res.failed(): " + res.failed());
				if (res.succeeded()) {
					System.out.println("QAReadService.start() SUCESSSS");
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					System.out.println("QAReadService.start() res.cause(): " + res.cause());
					System.out.println("QAReadService.start() FAILED: " + res.cause().getLocalizedMessage());
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
					// JsonObject jsonErrObj = new JsonObject();
					// jsonErrObj.put("FAIL_CAUSE", "NO QUESTIONS FOUND");
					// rctx.response().setStatusCode(404).putHeader("Content-Type",
					// "application/json")
					// .end(jsonErrObj.encodePrettily());
				}
			});
		});

		router.get("/questions/:questionId").handler(rctx -> {
			System.out.println("QAReadService.start() questions/:questionId: " + rctx.request().getParam("questionId"));
			JsonObject jsonObj = new JsonObject().put("questionId", rctx.request().getParam("questionId"));
			vertx.eventBus().send("read.Question", jsonObj.encodePrettily(), res -> {
				if (res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
							.end(res.result().body().toString());
				} else if (res.failed()) {
					rctx.response().setStatusCode(((ReplyException) res.cause()).failureCode())
							.putHeader("Content-Type", "application/json").end(res.cause().getLocalizedMessage());
				}
			});
		});

		router.post("/questions/").handler(rctx -> {
			System.out.println("QAReadService.start() POST QUESTION");
			getHttpClient_for_cloud(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("QAReadService.start() FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization_for_cloud(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {

							@Override
							public void handle(AsyncResult<Object> result) {
								System.out.println("QAReadService.start(...).new Handler() {...}.handle() result: "
										+ result.result());
								System.out.println(
										"QAReadService.start(...).new Handler() {...}.handle() result.result() instanceof String: "
												+ (result.result() instanceof String));
								if (result.result() instanceof String) {
									String response = (String) result.result();
									JsonObject usernameObj = getUserObject(response);
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									System.out.println(
											"QAReadService.start(...).new Handler() {...}.handle() statusCode: "
													+ statusCode);
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										questionObj.put("user", usernameObj);
										questionObj.put("postedDate", new Date().toString());
										System.out.println(
												"QAReadService.start() questionObj: " + questionObj.encodePrettily());

										vertx.eventBus().send("post.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});

		});
		
		router.patch("/questions/:questionId/edit/").handler(rctx -> {
			System.out.println("QAReadService.start() EDIT QUESTION");
			getHttpClient_for_cloud(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("QAReadService.start() FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization_for_cloud(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof String) {
									String response = (String) result.result();
									JsonObject usernameObj = getUserObject(response);
									int statusCode = usernameObj.getInteger("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put("lastUpdated", new Date().toString());
										vertx.eventBus().send("edit.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});
		
		router.patch("/questions/:questionId/comment/").handler(rctx -> {
			System.out.println("QAReadService.start() COMMENT QUESTION");

			getHttpClient_for_cloud(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("QAReadService.start() FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization_for_cloud(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof String) {
									String response = (String) result.result();
									JsonObject usernameObj = getUserObject(response);
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										questionObj.put("user", usernameObj);
										questionObj.put("postedDate", new Date().toString());
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										vertx.eventBus().send("comment.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});

		});
		
		
		router.patch("/questions/:questionId/answer/").handler(rctx -> {
			System.out.println("QAReadService.start() ANSWER TO QUESTION");
			getHttpClient_for_cloud(new Handler<AsyncResult<Object>>() {
				@Override
				public void handle(AsyncResult<Object> result) {
					if (result.failed()) {
						System.out.println("QAReadService.start() FAILURE MESSAGE: " + result.result());
					} else {
						System.out.println("UserService Server Discovered!");
						HttpClient client = (HttpClient) result.result();
						doAuthorization_for_cloud(rctx, client, "/questions/", new Handler<AsyncResult<Object>>() {
							@Override
							public void handle(AsyncResult<Object> result) {
								if (result.result() instanceof String) {
									String response = (String) result.result();
									JsonObject usernameObj = getUserObject(response);
									int statusCode = usernameObj.getInteger("statuscode");
									usernameObj.remove("statuscode");
									if (statusCode == 404) {
										JsonObject questionObj = rctx.getBodyAsJson();
										questionObj.put("user", usernameObj);
										questionObj.put("questionId", rctx.request().getParam("questionId"));
										questionObj.put("postedDate", new Date().toString());
										vertx.eventBus().send("answer.question", questionObj.encodePrettily(), res -> {
											if (res.succeeded()) {
												rctx.response().setStatusCode(200)
														.putHeader("Content-Type", "application/json")
														.end(res.result().body().toString());
											} else if (res.failed()) {
												rctx.response()
														.setStatusCode(((ReplyException) res.cause()).failureCode())
														.putHeader("Content-Type", "application/json")
														.end(res.cause().getLocalizedMessage());
											}
										});
									} else {
										rctx.response().setStatusCode(statusCode).putHeader("Content-Type", "text/html")
												.end("Authorization Failed");
									}
								}
							}
						});
					}
				}
			});
		});

//		createHttpServer(router, host, port)
//				.compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port, "/questions/"));
		
		vertx.createHttpServer().requestHandler(router::accept).listen(DEFAULT_PORT);

	}
	
	private JsonObject getUserObject(String response) {
		StringTokenizer tokenizer = new StringTokenizer(response, ":");
		int statusCode = 0;
		String username = "";
		int count = 0;
		while (tokenizer.hasMoreElements()) {
			String object = (String) tokenizer.nextElement();
			if (count == 0) {
				statusCode = Integer.parseInt(object);
			} else if (count == 1) {
				username = object;
			}
			count++;
		}
		JsonObject usernameObj = new JsonObject();
		usernameObj.put("username", username);
		usernameObj.put("statuscode", statusCode);
		return usernameObj;
	}
	
	private void getHttpClient_for_cloud(Handler<AsyncResult<Object>> handler) {
		handler.handle(Future.succeededFuture(vertx.createHttpClient()));
	}
	
	private void getHttpClient_for_staging(Handler<AsyncResult<Object>> handler) {
		HttpEndpoint.getClient(discovery, new JsonObject().put("name", "UserService"), ar -> {
			if (ar.failed()) {
				System.out.println("UserService Server Not Ready!");
				handler.handle(Future.failedFuture("UserService Server Not Ready!"));
//				return null;
			} else {
				HttpClient client = ar.result();
				handler.handle(Future.succeededFuture(client));
			}
		});
	}

	private void doAuthorization(RoutingContext context, HttpClient client, String path,
			Handler<AsyncResult<Object>> handler) {
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {
			HttpClientRequest toReq = client.request(context.request().method(), path, response -> {
				response.bodyHandler(body -> {
					System.out
							.println("QAReadService.doAuthorization() response.statusCode(): " + response.statusCode());
					if (response.statusCode() >= 500) { // api endpoint server
														// error, circuit
														// breaker
														// should fail
						future.fail(response.statusCode() + ": " + body.toString());
						handler.handle(Future.failedFuture(response.statusCode() + ": " + body.toString()));
					} else {
						// HttpServerResponse toRsp =
						// context.response().setStatusCode(response.statusCode());
						// response.headers().forEach(header -> {
						// System.out.println("QAReadService.doAuthorization()
						// header.getKey(): "+header.getKey());
						// System.out.println("QAReadService.doAuthorization()
						// header.getValue(): "+header.getValue());
						// toRsp.putHeader(header.getKey(), header.getValue());
						// });
						System.out.println("QAReadService.doAuthorization() body: " + body);
						// toRsp.setStatusMessage(body.toJsonObject().encodePrettily());
						// send response
						// toRsp.end(body);
						JsonObject usernameObj = body.toJsonObject();
						System.out.println("QAReadService.doAuthorization()" + usernameObj.getString("username"));
						handler.handle(Future
								.succeededFuture(response.statusCode() + ":" + usernameObj.getString("username")));
						future.complete();
					}
					ServiceDiscovery.releaseServiceObject(discovery, client);
				});
			});
			// set headers
			context.request().headers().forEach(header -> {
				toReq.putHeader(header.getKey(), header.getValue());
			});
			if (context.user() != null) {
				toReq.putHeader("user-principal", context.user().principal().encode());
			}
			System.out.println("QAReadService.doAuthorization() context.getBody(): " + context.getBody());
			// send request
			if (context.getBody() == null) {
				toReq.end();
			} else {
				toReq.end(context.getBody());
			}
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
				handler.handle(Future.failedFuture("bad_gateway"));
			}
		});
	}
	
	private void doAuthorization_for_cloud(RoutingContext context, HttpClient client, String path,
			Handler<AsyncResult<Object>> handler) {
		// run with circuit breaker in order to deal with failure
		circuitBreaker.execute(future -> {
			HttpClientRequest toReq = client.request(context.request().method(),80, USER_SERVICE_HOST, path, response -> {
				response.bodyHandler(body -> {
					System.out
							.println("QAReadService.doAuthorization() response.statusCode(): " + response.statusCode());
					if (response.statusCode() >= 500) { // api endpoint server
														// error, circuit
														// breaker
														// should fail
						future.fail(response.statusCode() + ": " + body.toString());
						handler.handle(Future.failedFuture(response.statusCode() + ": " + body.toString()));
					} else {
						// HttpServerResponse toRsp =
						// context.response().setStatusCode(response.statusCode());
						// response.headers().forEach(header -> {
						// System.out.println("QAReadService.doAuthorization()
						// header.getKey(): "+header.getKey());
						// System.out.println("QAReadService.doAuthorization()
						// header.getValue(): "+header.getValue());
						// toRsp.putHeader(header.getKey(), header.getValue());
						// });
						System.out.println("QAReadService.doAuthorization() body: " + body);
						// toRsp.setStatusMessage(body.toJsonObject().encodePrettily());
						// send response
						// toRsp.end(body);
						JsonObject usernameObj = body.toJsonObject();
						System.out.println("QAReadService.doAuthorization()" + usernameObj.getString("username"));
						handler.handle(Future
								.succeededFuture(response.statusCode() + ":" + usernameObj.getString("username")));
						future.complete();
					}
					ServiceDiscovery.releaseServiceObject(discovery, client);
				});
			});
			// set headers
			context.request().headers().forEach(header -> {
				toReq.putHeader(header.getKey(), header.getValue());
			});
			if (context.user() != null) {
				toReq.putHeader("user-principal", context.user().principal().encode());
			}
			System.out.println("QAReadService.doAuthorization() context.getBody(): " + context.getBody());
			// send request
			if (context.getBody() == null) {
				toReq.end();
			} else {
				toReq.end(context.getBody());
			}
		}).setHandler(ar -> {
			if (ar.failed()) {
				badGateway(ar.cause(), context);
				handler.handle(Future.failedFuture("bad_gateway"));
			}
		});
	}

}
